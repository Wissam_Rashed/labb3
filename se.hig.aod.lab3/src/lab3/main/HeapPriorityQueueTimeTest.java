package lab3.main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import se.hig.aod.lab3.BSTPriorityQueue;
import se.hig.aod.lab3.DuplicateItemException;
import se.hig.aod.lab3.EmptyQueueException;
import se.hig.aod.lab3.HeapPriorityQueue;

public class HeapPriorityQueueTimeTest {
	
	public static void main(String[] args) throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {
		
		HeapPriorityQueueTimeTest timeTest = new HeapPriorityQueueTimeTest();
	
		timeTest.heapNotSorted();
		timeTest.heapSorted();
		timeTest.heapSortedReversed();
		
		timeTest.bstNotSorted();
		timeTest.bstSorted();
		timeTest.bstSortedReversed();


	}
	
	public void heapNotSorted() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> newData = loadList("data_640000.txt", 640000);
		HeapPriorityQueue<Integer> heap = new HeapPriorityQueue<Integer>();
		for (Integer num : newData) {
			heap.enqueue(num);
		}
		
		List<Integer> newData2 = loadList("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer num : newData2) {
			heap.enqueue(num);
		}
		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			heap.dequeue();
		}
		long lastTime = System.currentTimeMillis();
		
		System.out.println("Time Heap for enqueue not sroted is " + (timeAfter - timeBefore) + "ms");
		System.out.println("Time Heap for dequeue not sroted is " + (lastTime - timeAfter) + "ms");
	}

	
	public void heapSorted() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> newData = loadList("data_640000.txt", 640000);
		HeapPriorityQueue<Integer> heap = new HeapPriorityQueue<Integer>();
		Collections.sort(newData);
		for (Integer num : newData) {
			heap.enqueue(num);
		}
		
		List<Integer> newData2 = loadList("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer num : newData2) {
			heap.enqueue(num);
		}
		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			heap.dequeue();
		}
		long lastTime = System.currentTimeMillis();
		System.out.println("Time Heap for enqueue sroted is " + (timeAfter - timeBefore) + "ms");
		System.out.println("Time Heap for dequeue sroted is " + (lastTime - timeAfter) + "ms");
	}

	
	public void heapSortedReversed() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> newData = loadList("data_640000.txt", 640000);
		HeapPriorityQueue<Integer> heap = new HeapPriorityQueue<Integer>();
		Collections.sort(newData, Collections.reverseOrder());
		for (Integer number : newData) {
			heap.enqueue(number);
		}
		List<Integer> newData2 = loadList("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer num : newData2) {
			heap.enqueue(num);
		}

		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			heap.dequeue();
		}
		long lastTime = System.currentTimeMillis();
		System.out.println("time Heap for enqueue reserved sroted is " + (timeAfter - timeBefore) + "ms");
		System.out.println("time Heap for dequeue reserved sroted is " + (lastTime - timeAfter) + "ms");
	}
	
	
	public void bstNotSorted() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> newData = loadList("data_640000.txt", 640000);
		BSTPriorityQueue<Integer> bst = new BSTPriorityQueue<Integer>();
		for (Integer num : newData) {
			bst.enqueue(num);
		}
		
		List<Integer> newData2 = loadList("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer num : newData2) {
			bst.enqueue(num);
		}
		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			bst.dequeue();
		}
		long lastTime = System.currentTimeMillis();
		
		System.out.println("Time BST for enqueue not sroted is: " + (timeAfter - timeBefore) + "ms");
		System.out.println("Time BST for dequeue not sroted is: " + (lastTime - timeAfter) + "ms");
	}

	
	public void bstSorted() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> newData = loadList("data_640000.txt", 640000);
		BSTPriorityQueue<Integer> bst = new BSTPriorityQueue<Integer>();
		Collections.sort(newData);
		for (Integer num : newData) {
			bst.enqueue(num);
		}
		
		List<Integer> newData2 = loadList("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer num : newData2) {
			bst.enqueue(num);
		}
		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			bst.dequeue();
		}
		long lastTime = System.currentTimeMillis();
		
		System.out.println("Time BST for enqueue sroted is " + (timeAfter - timeBefore) + "ms");
		System.out.println("Time BST for dequeue sroted is " + (lastTime - timeAfter) + "ms");
	}

	
	public void bstSortedReversed() throws FileNotFoundException, IOException, DuplicateItemException, EmptyQueueException {

		List<Integer> newData = loadList("data_640000.txt", 640000);
		BSTPriorityQueue<Integer> bst = new BSTPriorityQueue<Integer>();
		
		Collections.sort(newData, Collections.reverseOrder());
		for (Integer num : newData) {
			bst.enqueue(num);
		}
		List<Integer> newData2 = loadList("data_6400.txt", 6400);
		long timeBefore = System.currentTimeMillis();
		for (Integer num : newData2) {
			bst.enqueue(num);
		}

		long timeAfter = System.currentTimeMillis();
		for (int i = 0; i < 6400; i++) {
			bst.dequeue();
		}
		long afterAfter = System.currentTimeMillis();
		System.out.println("Time BST for enqueue reserved sroted is " + (timeAfter - timeBefore) + "ms");
		System.out.println("Time BST for dequeue reserved sroted is " + (afterAfter - timeAfter) + "ms");
	}

	private List<Integer> loadList(String path, int size)
			 throws FileNotFoundException, IOException {
			 List<Integer> list = new ArrayList<Integer>();
			 int cnt = 0;
			 String l;
			 try (BufferedReader in = new BufferedReader(new FileReader(path))) {
			 while ((l = in.readLine()) != null && cnt < size) {
			 list.add(Integer.parseInt(l));
			 cnt++;
			 }
			 in.close();

			 } // här har 
			 // garanterat anropats
			 
			 return list;
			}
	public boolean isFull() {
		// TODO Auto-generated method stub
		return false;
	}
}