package lab3.main;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HeapPriorityQueueTest2 {

	PriorityQueue<Integer> test;

	@BeforeEach
	void setUp() throws Exception {
		test = new HeapPriorityQueue<Integer>(5);
	}

	@AfterEach
	void tearDown() throws Exception {
		test = null;
	}

	@Test
	void isEmptyTest() {
		test.clear();
		assertTrue(test.isEmpty(), "It is empty");

	}

	@Test
	void isFullTest() {
		test.enqueue(1);
		test.enqueue(2);
		test.enqueue(3);
		test.enqueue(4);
		test.enqueue(5);
		assertTrue(test.isFull(), "It is full");
	}

	@Test
	void clearTest() {
		test.enqueue(1);
		test.enqueue(2);
		assertFalse(test.isEmpty(), "It is not empty");
		test.clear();
		assertTrue(test.isEmpty(), "It is empty");
	}

	@Test
	void enqueueTest() {
		test.enqueue(1);
		test.enqueue(2);
		assertEquals(test.dequeue(), 1);
	}

	@Test
	void dequeueTest() {
		test.enqueue(5);
		test.enqueue(1);
		test.enqueue(10);
		assertEquals(test.dequeue(), 1);
	}

	@Test
	void getFrontTest() {
		test.enqueue(5);
		test.enqueue(1);
		test.enqueue(10);
		assertEquals(test.getFront(), 1);
		test.dequeue();
		assertEquals(test.getFront(), 5);
	}

	@Test
	void size() {
		assertEquals(test.size(), 0);
		test.enqueue(5);
		test.enqueue(1);
		test.enqueue(10);
		assertEquals(test.size(), 3);
		test.dequeue();
		assertEquals(test.size(), 2);
		test.clear();
		assertEquals(test.size(), 0);
		assertFalse(test.isFull(), "false");

	}

	@Test
	void diffrentsFelTest() {

		assertThrows(RuntimeException.class, () -> test.getFront());

		assertThrows(RuntimeException.class, () -> test.dequeue());

		test.enqueue(1);
		test.enqueue(2);
		test.enqueue(3);
		test.enqueue(4);
		test.enqueue(5);

		assertThrows(RuntimeException.class, () -> test.enqueue(6));

		assertThrows(RuntimeException.class, () -> test = new HeapPriorityQueue<Integer>(-2));
	}

}