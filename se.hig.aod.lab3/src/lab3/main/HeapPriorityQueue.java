package lab3.main;

public class HeapPriorityQueue<T extends Comparable<T>> implements PriorityQueue<T> {
	private int size;
	private int currentSize;
	private int rear;
	private Object[] array;

	public HeapPriorityQueue(int size) {
		if (size > 0) {
			rear = -1;
			this.size = size;
			array = new Object[size];
			this.currentSize = 0;
		} else
			throw new RuntimeException(" Size can not be less than zero");
	}

	@Override
	public void clear() {
		size = 0;
		currentSize = 0;
	}

	@Override
	public boolean isEmpty() {
		if (currentSize == 0) {
			return true;
		}

		else {
			return false;
		}
	}

	@Override
	public boolean isFull() {

		if (currentSize == array.length) {
			return true;
		} else {
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public void enqueue(Comparable t) {

		if (isFull()) {
			throw new RuntimeException("Can not enqueue, it is full!");
		} else {
			rear = (rear + 1);
			array[rear] = (T) t;
			currentSize++;
			risingHeap();
		}
	}

	@SuppressWarnings("unchecked")
	private void risingHeap() {

		int jumpUpElement = rear;
		while (jumpUpElement > 0) {
			int jumpUpElement2 = (jumpUpElement - 1) / 2;

			T object = (T) array[jumpUpElement];

			T parent = (T) array[jumpUpElement2];

			if (object.compareTo(parent) < 0) {

				array[jumpUpElement] = parent;
				array[jumpUpElement2] = object;
				jumpUpElement = jumpUpElement2;

			} else
				return;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T dequeue() {

		T dequeueLowest = null;
		if (isEmpty()) {
			throw new RuntimeException("Can not dequeue, it is empty!!");
		}
		if (currentSize == 1) {

			Object tempArray = array[0];
			array[0] = null;
			currentSize--;
			rear--;
			return (T) tempArray;

		} else {
			dequeueLowest = (T) array[0];

			array[0] = array[rear];
			currentSize--;
			rear--;
			bottomHeap();
		}
		return dequeueLowest;
	}

	@SuppressWarnings("unchecked")
	private void bottomHeap() {

		int saveValue = 0;
		int childL = 2 * saveValue + 1;

		while (childL < currentSize) {
			int smallestElements = childL;
			int childR = childL + 1;
			if (childR < currentSize) {
				if (((Comparable<T>) array[childR]).compareTo((T) array[childL]) < 0) {
					smallestElements++;
				}
			}

			if (((Comparable<T>) array[saveValue]).compareTo((T) array[smallestElements]) > 0) {

				T temp = (T) array[saveValue];
				array[saveValue] = array[smallestElements];
				array[smallestElements] = temp;
				saveValue = smallestElements;
				childL = 2 * saveValue + 1;

			} else
				return;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getFront() throws RuntimeException {

		if (isEmpty()) {
			throw new RuntimeException("Error, it is empty!");
		} else {
			return (T) array[0];
		}
	}

	@Override
	public int size() {
		return currentSize;
	}
}
