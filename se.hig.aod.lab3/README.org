Högskolan i Gävle

Laboration 3: Prioritetsköer

Wissam Rashed
wissam.r.95@outlook.com
Rawand Al Kilani
rawandkelanyy@gmail.com
2020/12/9


Diskussionsfrågor:
1.	PriorityQueue bör hantera dubbletter.
	I fall vill man undvika dubbletter och se till att det inte finns två identiska objekt så skulle det kanske skriva en kod som körs parallellt med PriorityQueue.
	 Det kan gå genom hela insättningar, ta bort dubbletter och upprätthålla PriorityQueue.
2.	 Enqueue-operation
A  Värsta fall för enqueue-opreation i list är O (1), genomsnittsfallet är O (1)

	B  Värsta fall för enqueue-opreation i BST är O (n), genomsnittsfallet är O(log n)

	C  Värsta fall för enqueue-opreation i är O(log n), genomsnittsfallet är O(log n)
3.	Dequeue-operation
	A  Värsta fall för dequeue-opreation i list är O (1), genomsnittsfallet är O (1)

	B  Värsta fall för dequeue-opreation i BST är O (n), genomsnittsfallet är O(log n)

	C  Värsta fall för dequeue-opreation i är O(log n), genomsnittsfallet är O(log n)

4.	
					Heap 									||				BST
------------------------------------------------------------||-------------------------------------------------------------					
		Osort	Osort	Sort	Sort	Sort/omv	Sort/omv||	Osort	Osort	sort		sort	Sort/omv	Sort/omv
------------------------------------------------------------||-------------------------------------------------------------
		Ins		Utt		Ins		Utt		Ins			Utt		||	Ins		Utt		Ins			Utt		Ins			Utt
------------------------------------------------------------||-------------------------------------------------------------
10000	4		8		1		5		2			1		||	3		1		238			0		202			333
20000	6		8		3		2		3			2		||	4		7		426			0		513			700
40000	6		9		7		1		9			2		||	4		1		1678		0		1502		1635
80000	4		6		9		4		10			1		||	4		2		5589		0		5347		4232
160000	27		17		15		2		17			2		||	6		1		8909		0		13135		10010
320000	36		10		40		3		62			2		||	7		1		25277		1		30110		31472
640000	74		12		59		4		101			3		||	10		1		53570		0		73367		59330
 	




Kurs: Algoritmer och datastrukturer 

Lärare: Anders Jackson, Atique Ullah och Hanna Holmgren

1	Inledning
Laborationen handlar om att bli bekant med den abstrakta datatypen prioritetskö både från programmeringens och analysens synvinkel. Därför omfattar den uppgifter som ger färdigheter i att utveckla ett
programmerings-gränssnitt till datatypen och implementera gränssnittet i form av en Java-klass. Under
laborationens gång ska två olika implementeringar av datatypen undersökas experimentellt. Syfte är
att visa tidsåtgången när datamängder med olika storlekar bearbetas samt diskutera resultaten för att
utveckla kunskaper om hur valet av datastrukturen kan influera prestandan av implementationen.


2	Metod och genomförande
2.1	Uppgift 1. Tidsåtgång för prioritetskö 

vi ska undersöka hur tidsåtgången ändrar sig med växande storlek av värden som finns i kön när det
sätts in ett konstant antal värden i kön eller tas bort från den. I föreläsning 8 kan du läsa att tidsåtgången för både insättning och borttagning är O(log n) ifall en heap används. För ett binärt sökträd är
det också O(log n) under förutsättning att trädet är välbalanserat.

Lösningen kommer att utgå från att skapa en tabell över antalet element i kön och motsvarande tidsåtgång för insättning/borttagning
genom att använda data som följande:  5000, 10000, 20000, 40000, 80000, 160000, 320000, 460000.


2.2	Uppgift 2. Interface för prioritetskö

Uppgift 2 kommer att utföras genom att  Java-interface för den abstrakta generiska datatypen Prioritetskö. Prioritetskön ska kunna
lagra data som implementerar Javas interface Comparable.
Detta för att datat i Prioritetskön skall
ordnas efter compareTo()-metoden i Comparable.
metoderna som ska finnas i interfacet är : clear, isEmpty, isFull,size, enqueue,dequeue och getFront.


2.3	Uppgift 3. Testprogram för prioritetskö

Uppgift 3 kommer att utföras genom att skapa ett testprogram som kan användas för att testa de metoder som finns i interfacet.
Detta sker genom att skapa lämpliga testfall för att testa köns operationer. Beskriv testfallen i rapportens resultatdel. Exempel på lämpliga testfall:
Ogiltiga operationer på en tom kö eller fylld kö – t.ex. ta bort element, skriv ut element, titta
på första element, osv. 

 


2.4	Implementationer av prioritetskö
Jag väljer uppgift (a)

uppgiften är att skriva klassen HeapPriorityQueue som en heap-baserad implementation av en prioritetskö. Heapar
lämpar sig bra för implementation av prioritetsköer eftersom de fungerar så att det minsta (alternativt
det största, beroende på hur man väljer att implementera heapen) elementet ligger högst upp i heapen.







